SHELL:=/bin/bash

apply:
	kubectl apply -f <(istioctl kube-inject -f helloworld-jwt.yaml) && \
	kubectl apply -f helloworld-jwt-auth.yaml && \
	kubectl apply -f <(istioctl kube-inject -f helloworld-jwt-istio.yaml) && \
	kubectl apply -f <(istioctl kube-inject -f helloworld-jwt-enable.yaml)

# https://github.com/johanhaleby/kubetail
tail:
	kubetail -n istio-system --tail 5

clean:
	kubectl delete --all deployments,pods,gateway,virtualservice,destinationrule,meshpolicy,policy && \
	kubectl delete svc auth-service hello-service && \
	kubectl delete ServiceEntry --all